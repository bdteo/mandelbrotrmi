package mandelbrot;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

class MandelbrotDecompositionComputerRunnable implements Runnable {
  int width, height, iterations, threadCount;
  double minX, maxX, minY, maxY;
  MandelbrotComputerListener listener;
  MandelbrotDecompositionComputerListener completionListener;

  public MandelbrotDecompositionComputerRunnable(int width, int height, double minX, double maxX, double minY, double maxY, int iterations, MandelbrotComputerListener listener, int threadCount, MandelbrotDecompositionComputerListener completionListener) {
    this.width = width;
    this.height = height;
    this.iterations = iterations;
    this.listener = listener;

    this.minX = minX;
    this.maxX = maxX;
    this.minY = minY;
    this.maxY = maxY;

    this.threadCount = threadCount;
    this.completionListener = completionListener;
  }

  public void run() {
    long startTime = System.nanoTime();
    long elapsed   = System.nanoTime();

    if (threadCount <= 1) {
      MandelbrotComputerRunnable task = new MandelbrotComputerRunnable(width, height, minX, maxX, minY, maxY, iterations, 0, listener);
      Thread t = new Thread(task);
      t.start();
      
      try { t.join(); }
      catch (InterruptedException e) {}
      

      elapsed = System.nanoTime() - startTime;
    }
    else {
      ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadCount);

      double deltaY = maxY - minY;
      double stepY  = deltaY / height;

      for (int i = 0; i < height; i++) {
        executor.execute(new MandelbrotComputerRunnable(width, 1, minX, maxX, minY + i * stepY, minY + (1 + i) * stepY, iterations, i, listener));
      }

      executor.shutdown();

      try {
        executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        elapsed = System.nanoTime() - startTime;
      } catch (InterruptedException e) {}
    }

    completionListener.onMandelbrotCompleted(elapsed);
  }
}