package mandelbrot;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.awt.Graphics;
import java.awt.Dimension;
import java.util.Random;

public class MandelbrotCanvas extends JPanel{
  private BufferedImage image, clearImage;

  public MandelbrotCanvas(int width, int height) {
    clearImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    image      = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
    setPreferredSize(new Dimension(width, height));
  }

  public void setPixel(int x, int y, int color) {
    image.setRGB(x, y, color);
    repaint(x, y, 1, 1);
  }

  public void draw() {
    /*Graphics g = image.getGraphics();
    g.dispose();
    */
  }

  public void flush() {
    image.flush();
  }
  
  public void clear() {
    image.setData(clearImage.getRaster());
    repaint(0, 0, image.getWidth(), image.getHeight());
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.drawImage(image, 0, 0, null);
    g.dispose();
  }
}
