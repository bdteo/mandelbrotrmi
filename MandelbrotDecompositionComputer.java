package mandelbrot;

class MandelbrotDecompositionComputer {

  public void compute(int width, int height, double minX, double maxX, double minY, double maxY, int iterations, MandelbrotComputerListener listener, int threadCount, MandelbrotDecompositionComputerListener completionListener) {
    MandelbrotDecompositionComputerRunnable decompositor = new MandelbrotDecompositionComputerRunnable(width, height, minX, maxX, minY, maxY, iterations, listener, threadCount, completionListener);

    new Thread(decompositor).start();
  }

}
