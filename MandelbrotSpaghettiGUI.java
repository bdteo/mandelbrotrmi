package mandelbrot;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class MandelbrotSpaghettiGUI {

  private JFrame mainFrame;
  private JPanel controlPanel;

  public JTextField iterationsInput;
  public JTextField minXInput, maxXInput, minYInput, maxYInput;
  public JSpinner   threadSpinner;
  public JLabel threadStats;

  private MandelbrotCanvas canvas;

  private JButton computeButton;

  public JButton getComputeButton() {
    return computeButton;
  }

  public void setPixel(int x, int y, int color) {
    if (canvas == null) { return; }
    canvas.setPixel(x, y, color);
  }

  public void draw() {
    if (canvas == null) { return; }
    canvas.draw();
  }

  public void flush() {
    if (canvas == null) { return; }
    canvas.flush();
  }
  
  public void clear() {
    if (canvas == null) { return; }
    canvas.clear();
  }

  public MandelbrotSpaghettiGUI(){
    prepareGUI();
  }

  private void prepareGUI() {
    mainFrame = new JFrame("Mandlebrot GUI");
    mainFrame.setSize(1024, 960);
    mainFrame.setLayout(new FlowLayout());
    mainFrame.setResizable(false);
    //mainFrame.setBounds(0, 0, 1024, 768);

    JPanel controlTable = new JPanel();
    controlTable.setLayout(new GridLayout(2, 1));

    controlPanel = new JPanel();
    controlPanel.setLayout(new FlowLayout());
    controlPanel.setBackground(new Color(128,0,0));

    threadStats = new JLabel();
    
    canvas = new MandelbrotCanvas(800, 768);

    controlTable.add(controlPanel);
    controlTable.add(threadStats);    

    computeButton = new JButton("Compute");
    computeButton.setActionCommand("Compute");

    mainFrame.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent windowEvent){
        System.exit(0);
      }
    });


    iterationsInput = new JTextField(10);
    iterationsInput.setText("15");

    minXInput = new JTextField(10);
    minXInput.setText("-2.0");
    maxXInput = new JTextField(10);
    maxXInput.setText("2.0");
    minYInput = new JTextField(10);
    minYInput.setText("-2.0");
    maxYInput = new JTextField(10);
    maxYInput.setText("2.0");

    controlPanel.add(minXInput);
    controlPanel.add(maxXInput);
    controlPanel.add(minYInput);
    controlPanel.add(maxYInput);

    controlPanel.add(iterationsInput);

    threadSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 96, 1));
    controlPanel.add(threadSpinner);
    controlPanel.add(computeButton);

    mainFrame.add(controlTable);
    mainFrame.add(canvas);

    mainFrame.setVisible(true);
  }
}

