package mandelbrot;

import java.awt.event.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import javax.imageio.ImageIO;
import java.io.File;

class Main {
  public static void main(String[] args) {
    if (GraphicsEnvironment.isHeadless() || args.length > 0) {
      new CLIProvider().provide(args);
    } else {
      provideGUI();
    }
  }

  private static class MCListener implements MandelbrotComputerListener {
    private Map<Integer, Double> computationsProgress;

    public MCListener() {
      computationsProgress = new ConcurrentHashMap<Integer, Double>();
    }

    public void onComputationExecuted(int x, int y, int rgb, double completed, int threadId) {
      computationsProgress.put(threadId, completed);
    }

    public void onComputationsCompleted(int threadId) {
      computationsProgress.remove(threadId);
    }

    protected String aggregateProgress() {
      String progress = "";
      Iterator it = computationsProgress.entrySet().iterator();
      while (it.hasNext()) {
        Map.Entry pair = (Map.Entry)it.next();
        String   value = String.format("%04d", (int)Math.ceil(((Double)pair.getValue()).doubleValue() * 1000));
        progress += "T:" + value + " ";
      }

      return progress;
    }
  }


  private static class CLIProvider {
    BufferedImage image;
    String outputFile;
    boolean quiet;

    private class CLIMCListener /*extends MCListener*/ implements MandelbrotComputerListener {
      public void onComputationExecuted(int x, int y, int rgb, double completed, int threadId) {
        /*super.onComputationExecuted(x, y, rgb, completed, threadId);
        String progress = aggregateProgress();
        System.out.print(progress + "\r");*/
        image.setRGB(x, y, rgb);
      }

      public void onComputationsCompleted(int threadId) {}
    }

    private class CLIMDCTimeListener implements MandelbrotDecompositionComputerListener {
      public void onMandelbrotCompleted(long elapsed) {
        System.out.println((quiet ? "" : "time elapsed: ") + elapsed/1000/1000);

        if (!quiet) {
          System.out.println("Saving png...");
        }

        try {
          if (ImageIO.write(image, "png", new File(outputFile))) {
            if (!quiet) {System.out.println("Saved!");}
          }
        } catch (Exception e) {
          if (!quiet) {System.out.println("Failed.");}
        }
      }
    }

    private void provide(String[] args) {
      int width  = 640;
      int height = 480;

      double minX, maxX, minY, maxY;
      minX = minY = -2;
      maxX = maxY = 2;

      int threadCount = 1;
      int iterations  = 500;

      outputFile = "zad15.png";

      quiet = false;

      /*parse arguments {*/
      for (int i = 0; i < args.length; i++) {
        String commandArg = "";
        String arg        = args[i];

        if (Arrays.asList(new String[]{"-r", "-rect", "-s", "-size", "-o", "-output", "-t", "-tasks", "-i", "-iterations"}).contains(arg)) {
          if (i+1 < args.length) {
            ++i;
            commandArg = args[i];
          }
          else {
            System.out.println("Incorrect argument list. For proper usage see -help.");
            return;
          }
        }

        String invalidArgValMsg = "Incorrect argument value for " + arg + " command. For proper usage see -help.";

        switch (arg) {
          case "-r":
          case "-rect":
            String[] bounds = commandArg.split(":");
            try {
              minX = Double.parseDouble(bounds[0]);
              maxX = Double.parseDouble(bounds[1]);
              minY = Double.parseDouble(bounds[2]);
              maxY = Double.parseDouble(bounds[3]);
            }
            catch (Exception e) {
              //System.out.println(e);
              System.out.println(invalidArgValMsg);
              return;
            }
            break;
          case "-s":
          case "-size":
            String[] wh = commandArg.split("x");
            try {
              width  = Integer.parseInt(wh[0]);
              height = Integer.parseInt(wh[1]);
            }
            catch (Exception e) {
              System.out.println(invalidArgValMsg);
              return;
            }
            break;
          case "-o":
          case "-output":
            outputFile = commandArg;
            break;
          case "-q":
          case "-quiet":
            quiet = true;
            break;
          case "-t":
          case "-tasks":
            try {
              threadCount = Integer.parseInt(commandArg);
            }
            catch (Exception e) {
              System.out.println(invalidArgValMsg);
              return;
            }
            break;
          case "-i":
          case "-iterations":
            try {
              iterations = Integer.parseInt(commandArg);
            }
            catch (Exception e) {
              System.out.println(invalidArgValMsg);
              return;
            }
            break;
          case "-h":
          case "-help":
            System.out.println(
              "\n"
              + "Example command usage:\n"
              + "  -r -rect '­2.0:2.0:­2.0:2.0'\n"
              + "  -s -size '640x480'\n"
              + "  -o -output 'file.png'\n"
              + "  -t -tasks '16'\n"
              + "  -q -quiet\n"
              + "  -i -iterations '500'\n"
            );
            break;
          default:
            System.out.println("Invalid argument list. See -help for proper usage.");
            return;
        }
      }
      /*} parse arguments*/

      if (!quiet) {
        System.out.printf("\nw:%s h:%s minX:%s maxX:%s minY:%s maxY:%s iterations:%s threads:%s output:%s\n", width, height, minX, maxX, minY, maxY, iterations, threadCount, outputFile);
      }

      MandelbrotDecompositionComputer computer = new MandelbrotDecompositionComputer();
      image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

      computer.compute(width, height, minX, maxX, minY, maxY, iterations, new CLIMCListener(), threadCount, new CLIMDCTimeListener());
    }
  }

  private static class GUIMDCTimeListener implements MandelbrotDecompositionComputerListener {
    private MandelbrotSpaghettiGUI gui;

    public GUIMDCTimeListener(MandelbrotSpaghettiGUI gui) {
      this.gui = gui;
    }

    public void onMandelbrotCompleted(long elapsed) {
      gui.threadStats.setText("time elapsed: " + elapsed/1000/1000);
    }
  }

  private static class GUIMCListener /*extends MCListener*/ implements MandelbrotComputerListener {
    private MandelbrotSpaghettiGUI gui;

    public GUIMCListener(MandelbrotSpaghettiGUI gui) {
      this.gui = gui;
    }

    public void onComputationExecuted(int x, int y, int rgb, double completed, int threadId) {
      /*super.onComputationExecuted(x, y, rgb, completed, threadId);
      gui.threadStats.setText(aggregateProgress());*/
      gui.setPixel(x, y, rgb);
    }

    public void onComputationsCompleted(int threadId) {}
  }

  private static void provideGUI() {
    MandelbrotSpaghettiGUI gui = new MandelbrotSpaghettiGUI();
    MandelbrotDecompositionComputer computer = new MandelbrotDecompositionComputer();

    gui.getComputeButton().addMouseListener(new MouseAdapter(){
      public void mouseClicked(MouseEvent me) {
        gui.flush();
        gui.clear();


        int iterations;
        double minXInput, maxXInput, minYInput, maxYInput;

        try {
          String iterationsVal = gui.iterationsInput.getText();
          iterations = Integer.parseInt(iterationsVal);
        }
        catch (Exception e) { iterations = 15; }

        try { minXInput = Double.parseDouble(gui.minXInput.getText()); } catch (Exception e) { minXInput = -2; }

        try { maxXInput = Double.parseDouble(gui.maxXInput.getText()); } catch (Exception e) { maxXInput = 2; }

        try { minYInput = Double.parseDouble(gui.minYInput.getText()); } catch (Exception e) { minYInput = -2; }

        try { maxYInput = Double.parseDouble(gui.maxYInput.getText()); } catch (Exception e) { maxYInput = 2; }

        int threads = (int)gui.threadSpinner.getValue();

        computer.compute(800, 768, minXInput, maxXInput, minYInput, maxYInput, iterations, new GUIMCListener(gui), threads, new GUIMDCTimeListener(gui));
      }
    });
  }
}
