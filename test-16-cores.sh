for ((i=1; i<17; i++))
do
  echo $i threads average
  echo "java -jar app.jar -o 'outfile-benchmark.png' -t $i -q $1 $2 $3 $4 $5 $6 $7 $8"
  timeElapsedTotal=0
  for ((j=1; j<5; j++))
  do    
    timeElapsed=$(java -jar app.jar -o 'outfile-benchmark.png' -t $i -q $1 $2 $3 $4 $5 $6 $7 $8 | grep -o '[0-9]*')
    timeElapsedTotal=$(echo "$timeElapsedTotal+$timeElapsed" | bc)
  done
  echo $(echo "$timeElapsedTotal / 5" | bc)
done
