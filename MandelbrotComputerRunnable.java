package mandelbrot;

class MandelbrotComputerRunnable implements Runnable {
  private MandelbrotComputer computer;
  
  private int w, h, i, oh;
  private MandelbrotComputerListener l;
  private double minX, maxX, minY, maxY;

  public MandelbrotComputerRunnable(int width, int height, double minX, double maxX, double minY, double maxY, int iterations, int offsetH, MandelbrotComputerListener listener) {
    w = width;
    h = height;
    i = iterations;
    l = listener;
    
    oh = offsetH;
    
    this.minX = minX;
    this.maxX = maxX;
    this.minY = minY;
    this.maxY = maxY;
    
    computer = new MandelbrotComputer();    
  }

  public void run() {
    computer.compute(w, h, minX, maxX, minY, maxY, i, oh, (int)Thread.currentThread().getId(), l);
  }
}
