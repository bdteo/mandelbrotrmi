package mandelbrot;

public interface MandelbrotDecompositionComputerListener {
  public void onMandelbrotCompleted(long elapsed);
}
