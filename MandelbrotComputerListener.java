package mandelbrot;

public interface MandelbrotComputerListener {
  public void onComputationExecuted(int x, int y, int color, double completed, int threadId);

  public void onComputationsCompleted(int threadId);
}
