package mandelbrot;

import org.apache.commons.math3.complex.Complex;
import java.util.concurrent.Callable;
import java.awt.image.BufferedImage;

import java.io.File;
import javax.imageio.ImageIO;
import java.io.IOException;

import java.awt.Color;

import java.lang.*;

class MandelbrotComputer {
  private static BufferedImage gradient;
  private Complex z0 = new Complex(0,0);

  public MandelbrotComputer() {
    try {
     gradient = ImageIO.read(new File("gradient.bmp"));
    } catch (IOException e) {}
  }

  public void compute(int width, int height, double xMin, double xMax, double yMin, double yMax, int maxIterations, int offsetHeight, int threadId, MandelbrotComputerListener listener) {
    double maxAbsolute = 6;

    double deltaX = xMax - xMin;
    double deltaY = yMax - yMin;

    double ratioX = deltaX/width;
    double ratioY = deltaY/height;
    
    int resolution = width*height;

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        int iterations = 0;
        double absolute;
        boolean inSet = false;

        Complex c, z, m;
        z = z0;
        c = new Complex(xMin + x*ratioX, yMin + y * ratioY);

        do {
          iterations++;

          z = z.multiply(z).add(c);
          m = z.exp().subtract(c);

          absolute = m.abs();

          if (iterations == maxIterations) {
            inSet = true;
            break;
          }
        }
        while (absolute < maxAbsolute);

        absolute = Math.min(absolute, maxAbsolute * 2);

        int rgb = calculateRGBColor(inSet, iterations, maxIterations, absolute);

        listener.onComputationExecuted(x, y + offsetHeight, rgb, (double)(x + y*width) / (double)resolution, threadId);
      }
    }

    listener.onComputationsCompleted(threadId);
  }

  public int calculateRGBColor(boolean inSet, int iterations, double iterationsMax, double absolute) {
    int rgb;

    double iterationsRatio = iterations / iterationsMax;
    double smooth = (iterations + 1 - Math.log(Math.log(absolute))/Math.log(2)) / iterationsMax;
    //smooth = iterationsRatio;

    if (inSet) {
      rgb = new Color(0, 0, 0).getRGB();
    }
    else if (gradient == null) {
      int red   = 255;
      int green = (int)Math.min(smooth*255, 255);
      int blue  = 255;

      rgb = new Color(red, green, blue).getRGB();
    }
    else {
      double gradientWidth = gradient.getWidth();
      rgb = gradient.getRGB(Math.max(
        (int)Math.min(smooth * gradientWidth, gradientWidth) - 1,
        0
      ), 0);
    }

    return rgb;
  }
}
